(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [z] (- 10 z))
        z (comp f g)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (+ 15 x))
        g (fn [x] (- 3 x))
        z (comp f g)]
    (z 5)))

(defn función-comp-3
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z #{-1 2 -2 3 -3 4 -4})))

(defn función-comp-4
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z [-1 2 -2 3 -3 4 -4])))

(defn función-comp-5
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (map-entry? xs))
        z (comp f g)]
    (z '(-1 2 -2 3 -3 4 -4))))

(defn función-comp-6
  []
  (let [f (fn [xs] (keyword xs))
        g (fn [xs] (str xs))
        z (comp f g)]
    (z [10 20])))

(defn función-comp-7
  []
  (let [f (fn [xs] (second xs))
        g (fn [xs] (reverse xs))
        z (comp f g)]
    (z '(true 10 20 "hello"))))

(defn función-comp-8
  []
  (let [f (fn [x] (not x))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 10)))

(defn función-comp-9
  []
  (let [f (fn [x] (not x))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 1000)))

(defn función-comp-10
  []
  (let [f (fn [x] (str x))
        g (fn [x] (double x))
        z (comp f g)]
    (z (+ 1 2 3))))

(defn función-comp-11
  []
  (let [f (fn [s] (char? s))
        g (fn [s] (first s))
        z (comp f g)]
    (z "Clojure")))

(defn función-comp-12
  []
  (let [f (fn [xs] (ident? xs))
        g (fn [xs] (empty xs))
        z (comp f g)]
    (z [\a \b \c \d])))

(defn función-comp-13
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (list xs))
        z (comp f g)]
    (z {true false 1 2 \a \b})))

(defn función-comp-14
  []
  (let [f (fn [xs] (int? xs))
        g (fn [xs] (first xs))
        z (comp f g)]
    (z [1 2 3 4])))

(defn función-comp-15
  []
  (let [f (fn [x] (+ 2 x))
        g (fn [x] (* 4 x))
        z (comp f g)]
    (z 20)))

(defn función-comp-16
  []
  (let [f (fn [x] (+ 2 x))
        g (fn [x] (- 8 x))
        z (comp f g)]
    (z 4)))

(defn función-comp-17
  []
  (let [f (fn [x] (- 6 x))
        g (fn [x] (- 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-18
  []
  (let [f (fn [x] (* 1 x))
        g (fn [x] (* 2 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-19
  []
  (let [f (fn [x] (/ x 50))
        g (fn [x] (/ 3 x))
        z (comp f g)]
    (z 1000)))

(defn función-comp-20
  []
  (let [f (fn [x] (- x 4))
        g (fn [x] (+ x 4))
        z (comp f g)]
    (z 10)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-19)
(función-comp-18)
(función-comp-20)



(defn función-complement-1
  []
  (let [f (fn [xs] (map even? xs))
        z (complement f)]
    (z '(10 20 30 40))))

(defn función-complement-2
  []
  (let [f (fn [xs] (float? xs))
        z (complement f)]
    (z 60)))

(defn función-complement-3
  []
  (let [f (fn [s] (associative? s))
        z (complement f)]
    (z "TheBatman")))

(defn función-complement-4
  []
  (let [f (fn [xs] (boolean? xs))
        z (complement f)]
    (z [false true])))

(defn función-complement-5
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z {true \a \b \c})))

(defn función-complement-6
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z [\a \b \c])))

(defn función-complement-7
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z  10)))

(defn función-complement-8
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z 5)))

(defn función-complement-9
  []
  (let [f (fn [s] (number? s))
        z (complement f)]
    (z "Spiderman")))

(defn función-complement-10
  []
  (let [f (fn [s] (char? s))
        z (complement f)]
    (z \a)))

(defn función-complement-11
  []
  (let [f (fn [x] (neg? x))
        z (complement f)]
    (z -20)))

(defn función-complement-12
  []
  (let [f (fn [x] (rational? x))
        z (complement f)]
    (z 0/4)))

(defn función-complement-13
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z \a)))

(defn función-complement-14
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z false)))

(defn función-complement-15
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z -400)))

(defn función-complement-16
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 3)))

(defn función-complement-17
  []
  (let [f (fn [s] (string? s))
        z (complement f)]
    (z "Lapras")))

(defn función-complement-18
  []
  (let [f (fn [xs] (associative? xs))
        z (complement f)]
    (z #{60 20 50})))

(defn función-complement-19
  []
  (let [f (fn [s] (char? s))
        z (complement f)]
    (z 'a)))

(defn función-complement-20
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z '(77 77 77))))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)



(defn función-constantly-1
  []
  (let [x true
        z (constantly x)]
    (z false)))

(defn función-constantly-2
  []
  (let [s "Prueba"
        z (constantly s)]
    (z "test")))

(defn función-constantly-3
  []
  (let [s \a
        z (constantly s)]
    (z  'a)))

(defn función-constantly-4
  []
  (let [xs [\H \O \L \A]
        z (constantly xs)]
    (z  ["hola"])))

(defn función-constantly-5
  []
  (let [xs {:a 1 :b 2 :c 3}
        z (constantly xs)]
    (z  {1 2 3 4})))

(defn función-constantly-6
  []
  (let [xs '(true false)
        z (constantly xs)]
    (z ["false"])))

(defn función-constantly-7
  []
  (let [x #{1 2 3}
        z (constantly x)]
    (z  '(1 2 3))))

(defn función-constantly-8
  []
  (let [xs (+ 2 2) 
        z (constantly xs)]
    (z [:a 1])))

(defn función-constantly-9
  []
  (let [xs (string? "PLF")
        z (constantly xs)]
    (z  #{"Prog. Funcional"})))

(defn función-constantly-10
  []
  (let [xs (int? \a)
        z (constantly xs)]
    (z  '(\a \b))))

(defn función-constantly-11
  []
  (let [x 16.5
        z (constantly x)]
    (z  20)))

(defn función-constantly-12
  []
  (let [x 5/4
        z (constantly x)]
    (z  1)))

(defn función-constantly-13
  []
  (let [xs (- 10 6)
        z (constantly xs)]
    (z  '("numeros"))))

(defn función-constantly-14
  []
  (let [xs ['(1 2) '(2 3) {1 2}]
        z (constantly xs)]
    (z  [true])))

(defn función-constantly-15
  []
  (let [xs (rational? 2)  
        z (constantly xs)]
    (z  '(2 5 6 3))))

(defn función-constantly-16
  []
  (let [x 4.7M   
        z (constantly x)]
    (z 47)))

(defn función-constantly-17
  []
  (let [s  false
        z (constantly s)]
    (z  "falso")))

(defn función-constantly-18
  []
  (let [xs  [:x 1 :z 2 ]
        z (constantly xs)]
    (z  {:x  1 :y 2 })))

(defn función-constantly-19
  []
  (let [s "System.out.println()"
        z (constantly s)]
    (z "print")))

(defn función-constantly-20
  []
  (let [xs '(+ 5 25)
        z (constantly xs)]
    (z [* 4 5])))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)



(defn función-every-pred-1
  [] 
  (let [f (fn [xs] (true? xs))
        g (fn [xs] (char? xs))
        z (every-pred f g)]
    (z [true \a])))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z ["hola"])))

 (defn función-every-pred-3
   []
   (let [f (fn [x] (number? x))
         g (fn [x] (pos? x))
         z (every-pred f g)]
     (z 70)))

(defn función-every-pred-4
  []
  (let [f (fn [s] (char? s))
        g (fn [s] (char? s))
        z (every-pred f g)]
    (z \h)))

(defn función-every-pred-5
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (pos? x))
        z (every-pred f g)]
    (z 70)))

(defn función-every-pred-6
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (neg? x))
        z (every-pred f g)]
    (z -9)))

(defn función-every-pred-7
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        z (every-pred f g)]
    (z 80)))

(defn función-every-pred-8
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (neg? x))
        z (every-pred f g)]
    (z -95)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z -6.66)))

(defn función-every-pred-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z 665.5)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        z (every-pred f g)]
    (z true)))

(defn función-every-pred-12
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (boolean? x))
        z (every-pred f g)]
    (z 45)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (boolean? x))
        z (every-pred f g)]
    (z 3857)))

(defn función-every-pred-14
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (char? x))
        z (every-pred f g)]
    (z \1)))

(defn función-every-pred-15
  []
  (let [f (fn [xs] (keyword? xs))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z :z)))

(defn función-every-pred-16
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z '("1" "2" "3"))))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (associative? xs))
        z (every-pred f g)]
    (z [true false \a])))

(defn función-every-pred-18
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (associative? xs))
        z (every-pred f g)]
    (z {:a 1 :b 2})))

(defn función-every-pred-19
  []
  (let [f (fn [xs] (set? xs))
        g (fn [xs] (associative? xs))
        z (every-pred f g)]
    (z #{10 20 30})))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (associative? xs))
        z (every-pred f g)]
    (z '(:x 1 :y 2 :z 3))))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3) 
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)



(defn función-fnil-1
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        z (fnil f 7)]
    (z nil 6)))

(defn función-fnil-2
  []
  (let [f (fn [x y] (if (< x y) (* x y) (/ x y)))
        z (fnil f 100)]
    (z nil 20)))

(defn función-fnil-3
  []
  (let [f (fn [x y] (if (= x y) (- x y) (vector (x y))))
        z (fnil f 50)]
    (z nil 50)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (if (number? x) (+ x y) (- x y)))
        z (fnil f 50)]
    (z nil 5.6)))

(defn función-fnil-5
  []
  (let [f (fn [s c] (if (nil? s) (str c) (vector c)))
        z (fnil f nil)]
    (z nil ["hola" "mundo"])))

(defn función-fnil-6
  []
  (let [f (fn [x y] (if (number? x) (str y) (vector y)))
        z (fnil f 80)]
    (z nil 90000)))

(defn función-fnil-7
  []
  (let [f (fn [x y] (if (float? x) (list y) (vector y)))
        z (fnil f 600)]
    (z nil [90 50 60])))

(defn función-fnil-8
  []
  (let [f (fn [s] (str "Bienvenidos " s))
        z (fnil f nil)]
    (z "camaradas")))

(defn función-fnil-9
  []
  (let [f (fn  [x y] (* x y))
        z (fnil f 50)]
    (z nil 35)))

(defn función-fnil-10
  []
  (let [f (fn [x y] (take x y))
        z (fnil f 2 )]
    (z nil (take 5 (range 100 150)))))

(defn función-fnil-11
  []
  (let [f (fn  [x y] (/ x y))
        z (fnil f 100)]
    (z nil 5)))

(defn función-fnil-12
  []
  (let [f (fn  [x y] (* x y))
        z (fnil f 5)]
    (z nil 20)))

(defn función-fnil-13
  []
  (let [f (fn  [x y] (+ x y))
        z (fnil f 50)]
    (z nil 50)))

(defn función-fnil-14
  []
  (let [f (fn  [x y] (- x y))
        z (fnil f 200)]
    (z nil 100)))

(defn función-fnil-15
  []
  (let [f (fn  [x y z] (+ x y z))
        z (fnil f 200)]
    (z nil 100 300)))

(defn función-fnil-16
  []
  (let [f (fn [x] (x))
        z (fnil inc f)]
    (z 100)))

(defn función-fnil-17
  []
  (let [f (fn [s] (str "Bye" s))
        z (fnil f nil)]
    (z " everyone")))

(defn función-fnil-18
  []
  (let [f (fn [x y] (if (integer? x) (hash-set x) (inc y)))
        z (fnil f 1000000)]
    (z nil 29)))

(defn función-fnil-19
  []
  (let [f (fn [x y] (if (nat-int? x) (inc x) (dec y)))
        z (fnil f 29)]
    (z nil 30)))

(defn función-fnil-20
  []
  (let [f (fn [x y] (if (double? x) (str [x y "double"]) (str [x y "noDouble"])))
        z (fnil f 10)]
    (z nil 20)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)



(defn función-juxt-1
  []
  (let [f (fn [s] (first s))
        g (fn [s] (count s))
        z (juxt f g)]
    (z "Diego")))

(defn función-juxt-2
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (str xs))
        z (juxt g f)]
    (z [2 2 2 2])))

(defn función-juxt-3
  []
  (let [f (fn [xs] (take 3 xs))
        g (fn [xs] (drop 4 xs))
        z (juxt f g)]
    (z [1 2 3 4 5])))

(defn función-juxt-4
  []
  (let [f (fn [xs] (vector xs))
        g (fn [xs] (filter odd? xs))
        z (juxt g f)]
    (z #{3 4 5 6})))

(defn función-juxt-5
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (first xs))
        z (juxt g f)]
    (z [:a 1 :b 2])))

(defn función-juxt-6
  []
  (let [f (fn [x] (- 10 x))
        g (fn [x] (- 10 x))
        h (fn [x] (inc  x))
        z (juxt h g f)]
    (z 2)))

(defn función-juxt-7
  []
  (let [f (fn [xs] (filter char? xs))
        g (fn [xs] (count xs))
        h (fn [xs] (vector xs))
        z (juxt h g f)]
    (z #{1 "uno" 2 "dos" 3 "tres"})))

(defn función-juxt-8
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (list xs))
        h (fn [xs] (conj [10] xs))
        z (juxt h g f)]
    (z [60 50 40 50])))

(defn función-juxt-9
  []
  (let [f (fn [s] (take 2 s))
        g (fn [s] (reverse s))
        z (juxt g f)]
    (z "Saint seiya")))

(defn función-juxt-10
  []
  (let [f (fn [x] (range x))
        g (fn [x] (- 40 x))
        h (fn [x] (int? x))
        z (juxt f h g)]
    (z 75)))

(defn función-juxt-11
  []
  (let [f (fn [xs] (map key xs))
        g (fn [xs] (map val xs))
        z (juxt f g)]
    (z {:a \a :b \b :c \c})))

(defn función-juxt-12
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (map val xs))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 3})))

(defn función-juxt-13
  []
  (let [f (fn [s] (sort s))
        g (fn [s] (conj [\a \b] s))
        z (juxt f g)]
    (z "5432")))

(defn función-juxt-14
  []
  (let [f (fn [s] (sort s))
        g (fn [s] (conj [\a \b \c] s))
        z (juxt f g)]
    (z "Testing")))

(defn función-juxt-15
  []
  (let [f (fn [xs] (remove true? xs))
        g (fn [xs] (last xs))
        h (fn [xs] (filter boolean? xs))
        z (juxt g f h)]
    (z '(true \a true "num" false 0.5 true 1M))))

(defn función-juxt-16
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (map? xs))
        z (juxt g f h)]
    (z '("uno" "dos" "tres" "cuatro"))))

(defn función-juxt-17
  []
  (let [f (fn [xs] (take-last 2 xs))
        g (fn [xs] (update xs 2 inc))
        h (fn [xs] (count xs))
        z (juxt g f h)]
    (z [10 20 30 40])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [xs] (remove neg? xs))
        z (juxt g f)]
    (z #{-1 -2 -3 -4 5})))

(defn función-juxt-19
  []
  (let [f (fn [xs] (sort-by count xs))
        g (fn [xs] (string? xs))
        z (juxt f g)]
    (z ["Auron" "Bjean" "Calvoo" "Rubiuh"])))

(defn función-juxt-20
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (last xs))
        z (juxt g f)]
    (z [:a 1 :b 2])))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)



(defn función-partial-1
  []
  (let [f (fn [x] (+ 1000 x))
        z (partial f)]
    (z 5)))

(defn función-partial-2
  []
  (let [f (fn [x] (* 1000 x))
        z (partial f)]
    (z 5)))

(defn función-partial-3
  []
  (let [f (fn [x] (- 1000 x))
        z (partial f)]
    (z 10)))

(defn función-partial-4
  []
  (let [f (fn [x] (/ 1000 x))
        z (partial f)]
    (z 5)))

(defn función-partial-5
  []
  (let [f (fn [x] (/ 100 x))
        z (partial f)]
    (z 5)))

(defn función-partial-6
  []
  (let [f (fn [x] (+ 1000000 x))
        z (partial f)]
    (z 5)))

(defn función-partial-7
  []
  (let [f (fn [xs] (list x))
        z (partial f)]
    (z [454 5645])))

(defn función-partial-8
  []
  (let [f (fn [xs] (hash-map xs 5))
        z (partial f)]
    (z [10 20 30 40])))

(defn función-partial-9
  []
  (let [f (fn [s] (filter char? s))
        z (partial f)]
    (z "hola")))

(defn función-partial-10
  []
  (let [f (fn [s] (group-by char? s))
        z (partial f)]
    (z "ACDC")))

(defn función-partial-11
  []
  (let [f (fn [s] (sort-by char? s))
        z (partial f)]
    (z "DiegOn")))

(defn función-partial-12
  []
  (let [f (fn [s] (take-while char? s))
        z (partial f)]
    (z "THUNDER")))

(defn función-partial-13
  []
  (let [f (fn [s] (sequential? s))
        z (partial f)]
    (z "The avengers")))

(defn función-partial-14
  []
  (let [f (fn [s] (remove char? s))
        z (partial f)]
    (z "hola")))

(defn función-partial-15
  []
  (let [f (fn [xs] (filterv even? xs))
        z (partial f)]
    (z [10 20])))

(defn función-partial-16
  []
  (let [f (fn [xs] (filter neg? xs))
        z (partial f)]
    (z [-1 2 -3 4])))

(defn función-partial-17
  []
  (let [f (fn [xs] (filter pos? xs))
        z (partial f)]
    (z [-1 2 -3 40])))

(defn función-partial-18
  []
  (let [f (fn [xs] (take-while string? xs))
        z (partial f)]
    (z ["PLF" -10 20 -30 40])))

(defn función-partial-19
  []
  (let [f (fn [xs] (take-while number? xs))
        z (partial f)]
    (z [-10 5.5 -30 false true])))

(defn función-partial-20
  []
  (let [f (fn [xs] (take-while boolean? xs))
        z (partial f)]
    (z [false true -10 5.5])))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)



(defn función-some-fn-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (even? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 150)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (even? x))
        h (fn [x] (double? x))
        z (some-fn f g h)]
    (z -900)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (even? x))
        h (fn [x] (decimal? x))
        z (some-fn f g h)]
    (z -400)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (odd? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z -555)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (odd? x))
        h (fn [x] (double? x))
        z (some-fn f g h)]
    (z -800)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (odd? x))
        h (fn [x] (decimal? x))
        z (some-fn f g h)]
    (z -10000)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (even? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 1000)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (even? x))
        h (fn [x] (double? x))
        z (some-fn f g h)]
    (z -5000)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (even? x))
        h (fn [x] (decimal? x))
        z (some-fn f g h)]
    (z -9000)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (odd? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z -4500)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (odd? x))
        h (fn [x] (double? x))
        z (some-fn f g h)]
    (z -4650)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (odd? x))
        h (fn [x] (decimal? x))
        z (some-fn f g h)]
    (z 60060)))

(defn función-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (decimal? x))
        z (some-fn f g)]
    (z 56540)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 544654)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (double? x))
        z (some-fn f g)]
    (z 5454)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (decimal? x))
        z (some-fn f g)]
    (z 465457)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 6565)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        z (some-fn f g)]
    (z -5442)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 988)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z -5464)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20) 